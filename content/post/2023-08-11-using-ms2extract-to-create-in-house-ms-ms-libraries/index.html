---
title: M2extract Part 1 - Introduction to MS2extract package
author: Cristian Quiroz-Moreno
date: '2023-08-11'
slug: [MS2extract]
categories:
  - LC-MS
tags:
  - MS2extract
  - MS/MS libraries
  - LC-MS
  - DataViz
subtitle: 'Part 1 - Using MS2extract to create MS/MS libaries'
description: 'In this post we will explore the core pipeline of MS2extract to create in-house MS/MS libraries. We will go from raw MS/MS data to a MS/MS library, passing detecting masses and removing MS/MS background noise.'
image: ''
---



<div id="package-installation" class="section level1">
<h1>Package Installation</h1>
<p>As of today, the <a href="https://github.com/CooperstoneLab/MS2extract">MS2extract</a>
package can only be found at GitHub. Therefore, you can install this pacakge by:</p>
<pre class="r"><code># install.packages(&quot;devtools&quot;)
devtools::install_github(&quot;CooperstoneLab/MS2extract&quot;)</code></pre>
<p><img src="MS2extract_logo.png" width="30%" height="30%" style="display: block; margin: auto;" /></p>
</div>
<div id="introduction" class="section level1">
<h1>Introduction</h1>
<p>This vignette has the objective to introduce the <strong>MS2extract</strong>. The
main goal of this package is to provide a tool to create in-house MS/MS compound
libraries. Users can access a specific function help through the command
help(<em>[‘function name’]</em>). It is worth to note that this package is aimed in
the targeted extraction of MS/MS scans and it is not able to perform compound
match or annotation.</p>
<p>A simplified workflow is presented in Figure 1. Briefly, mzMl/mzXML files are
imported in memory, then based on metadata provided by the user such as
analyte chemical formula, and ionization mode,
MS2extract computes the theoretical precursor <em>m</em>/<em>z</em>. Then, product
ion scans that matches the theoretical analyte precursor ion
are extracted, with a given ppm tolerance.
Next, low intensity signals, or background noise can be removed
from the spectra. Finally, users can export the extracted MS/MS spectra to
a MS/MS library format (.msp/.mgf) to be used as reference library for
further compound identification and annotation, or deposit the created library
in different repositories such as GNPS, or MassBank.</p>
<div class="figure" style="text-align: center">
<img src="pipeline.png" alt="Figure 1. Overview of the processing pipeline of MS2extract" width="80%" height="80%" />
<p class="caption">
(#fig:Fig 1)Figure 1. Overview of the processing pipeline of MS2extract
</p>
</div>
</div>
<div id="basic-workflow" class="section level1">
<h1>Basic workflow</h1>
<p>The workflow has four main steps:</p>
<ul>
<li>data import,</li>
<li>extract MS/MS scans,</li>
<li>detect masses, and</li>
<li>export the MS/MS library</li>
</ul>
<p>In this section, we will explain in a more detailed manner the main steps to
create in-house MS/MS libraries,
as well as provide information about the required and optional arguments
that users may need to provide in order to effectively use this package.</p>
<p>Additionally, this package also includes a set of <code>batch_*()</code> functions that
allows to process multiple .mzXML files at once. However, more metadata is
required to run this automated pipeline and the use of this <code>batch_*()</code>
functions
will is described in the
<a href="https://cooperstonelab.github.io/MS2extract/articles/Busing_batch_extract.html">Using MS2extract Batch Pipeline</a>.</p>
<div id="data-import" class="section level2">
<h2>Data import</h2>
<p>This section is focused on describing how MS2extract package imports MS/MS data.
We also include a more detailed document about this process in the
<a href="https://cooperstonelab.github.io/MS2extract/articles/Cimport_mzml_explanation.html">Behind the curtains of importing MS/MS data</a>
vignette.</p>
<p>The main import function relies on R package
<a href="https://metid.tidymass.org">metID</a>. We adapted the import function in
order to read mass spectrometry data from mzML/mzXML files. The new adaptation
consists in importing scans data in a list (S3 object) rather than into a S4
object, facilitating the downstream tidy analysis of this object.</p>
<blockquote>
<p>This function execute a back-end calculation of theoretical ionized <em>m</em>/<em>z</em>
of the compound in order to extract the precursor ions that match that mass
with a given ppm.</p>
</blockquote>
<p>The arguments of the <code>import_mzxml()</code> functions are four:</p>
<ul>
<li><strong>file:</strong> mzML/mzXML file name</li>
<li><strong>met_metadata:</strong> metadata of the analyte</li>
<li><strong>ppm:</strong> error mass expressed in ppm</li>
<li>…</li>
</ul>
<pre class="r"><code># Loading the package
library(MS2extract)
#&gt; Warning in fun(libname, pkgname): mzR has been built against a different Rcpp version (1.0.10)
#&gt; than is installed on your system (1.0.11). This might lead to errors
#&gt; when loading mzR. If you encounter such issues, please send a report,
#&gt; including the output of sessionInfo() to the Bioc support forum at 
#&gt; https://support.bioconductor.org/. For details see also
#&gt; https://github.com/sneumann/mzR/wiki/mzR-Rcpp-compiler-linker-issue.


# Print function arg
formals(import_mzxml)
#&gt; $file
#&gt; NULL
#&gt; 
#&gt; $met_metadata
#&gt; NULL
#&gt; 
#&gt; $ppm
#&gt; [1] 10
#&gt; 
#&gt; $...</code></pre>
<div id="file" class="section level3">
<h3>file</h3>
<p>File should contain the name of your mzML/mzXML file that contains MS/MS data
of authentic standards or reference material. Here, we provide an example
file of procyanidin A2 collected in negative ionization mode, and a collision
energy of 20 eV.</p>
<pre class="r"><code># Importing  Procyanidin A2 MS/MS spectra in negative ionization mode
# and 20 eV as the collision energy
ProcA2_file &lt;- system.file(&quot;extdata&quot;,
  &quot;ProcyanidinA2_neg_20eV.mzXML&quot;,
  package = &quot;MS2extract&quot;
)
# File name
ProcA2_file
#&gt; [1] &quot;/Users/quirozmoreno.1/Library/R/arm64/4.3/library/MS2extract/extdata/ProcyanidinA2_neg_20eV.mzXML&quot;</code></pre>
</div>
<div id="met_metadata" class="section level3">
<h3>met_metadata</h3>
<p>This argument refers to the compound metadata that user need to provide in
order to properly import scans that are related to the compound of interest.</p>
<p>The <code>met_metadata</code> is a data frame that has required and optional columns. The
required columns are employed to calculate the theoretical ionized <em>m</em>/<em>z</em> for a
given formula and ionization mode. In the optional columns, we have the option
to provide a chromatographic region of Interest (ROI), specifying at what time
the the compound elutes, in order to only keep this retention time window.</p>
<p>The required columns are:</p>
<ul>
<li><strong>Formula:</strong> A character string specifying the metabolite formula</li>
<li><strong>Ionization_mode:</strong> The ionization mode employed in data collection.</li>
</ul>
<p>The optional columns are:</p>
<ul>
<li><strong>min_rt:</strong> a double with the minimum retention time to keep (in seconds)</li>
<li><strong>max_rt:</strong> a double with the minimum retention time to keep (in seconds)</li>
</ul>
<pre class="r"><code># Procyanidin A2 metadata
ProcA2_data &lt;- data.frame(
  Formula = &quot;C30H24O12&quot;, Ionization_mode = &quot;Negative&quot;,
  min_rt = 163, max_rt = 180
)
ProcA2_data
#&gt;     Formula Ionization_mode min_rt max_rt
#&gt; 1 C30H24O12        Negative    163    180</code></pre>
</div>
<div id="ppm" class="section level3">
<h3>ppm</h3>
<p>ppm refers to the maximum m/z deviation from the theoretical mass. A ppm of 10
units will mean that the total allows <em>m</em>/<em>z</em> window in 20 ppm.
By default, 10 ppm is used.</p>
</div>
<div id="import_mzxml" class="section level3">
<h3><code>import_mzxml()</code></h3>
<p>With all arguments explained, we can use the <code>import_mzxml()</code> function.</p>
<pre class="r"><code># Import Procyanidin A2 data
ProcA2_raw &lt;- import_mzxml(ProcA2_file, met_metadata = ProcA2_data, ppm = 5)
#&gt; • Processing: ProcyanidinA2_neg_20eV.mzXML
#&gt; • Found 1 CE value: 20
#&gt; • Remember to match CE velues in spec_metadata when exporting your library
#&gt; • m/z range given 5 ppm: 575.11663 and 575.12238

# 24249 rows = ions detected in all scans
dim(ProcA2_raw)
#&gt; [1] 24249     6</code></pre>
</div>
</div>
<div id="extracting-msms-spectra" class="section level2">
<h2>Extracting MS/MS spectra</h2>
<p>Now that we have the data imported, we can proceed to extract the
<strong>most intense MS/MS scan</strong>.</p>
<p>This function computes the MS/MS total ion chromatogram (TIC)
by summing up all intensities of the MS/MS
scans, and selects the scan with the highest total intensity. It is worth noting
that we only imported MS/MS scans that the precursor ion matches the
theoretical <em>m/z</em> value of the compound provided in the previous step.
Therefore, it is more accurrate to interpret this chromatogram as
an EIC of the precursor ion, where only the MS/MS scans are included.</p>
<p>This function takes three arguments:</p>
<ul>
<li><strong>spec:</strong> the imported MS/MS spectra</li>
<li><strong>verbose:</strong> a boolean, if <code>verbose = TRUE</code>, the MS/MS TIC and spectra is printed, if <code>verbose = FALSE</code>, plots are not displayed</li>
<li><strong>out_list:</strong> a boolean, if <code>out_list = TRUE</code>, the extracted MS/MS spectra table and plots are returned as list, otherwise only the MS/MS spectra is returned as data frame.</li>
</ul>
<pre class="r"><code>ProcA2_extracted &lt;- extract_MS2(ProcA2_raw, verbose = TRUE, out_list = FALSE)
#&gt; Warning: `position_stack()` requires non-overlapping x intervals</code></pre>
<p><img src="unnamed-chunk-8-1.png" width="100%" height="100%" /></p>
<p>We generated two plots, the MS/MS EIC of the precursor ion (top plot), and the
MS/MS spectra of the most intense MS/MS scan (bottom plot). In the MS/MS
spectra, the blue diamond is placed on top of the precursor <em>m/z</em> ion. If the
diamond is filled in blue, it means the precursor ion was found in the MS/MS
fragmentation data, while a diamond that is not filled will represent that the
precursor ion was not found in the fragmentation data.</p>
<p>Furthemore, we can note that the x axis in the MS/MS spectra ranges from 0 to
1700 <em>m/z</em>. This is more related to the acquisition parameters used in data
callection <em>m/z</em> range: 50-1700, which creates low intensity signal that are
captured and included in the resulting MS/MS spectra.</p>
<pre class="r"><code>range(ProcA2_extracted$mz)
#&gt; [1]  100.0852 1699.0981</code></pre>
<p>The range of the MS/MS <em>m</em>/<em>z</em> values are from 100 to 1699 <em>m</em>/<em>z</em>, but
intensities are too low to be seen in the plot.</p>
</div>
<div id="detecting-masses" class="section level2">
<h2>Detecting masses</h2>
<p>Similarly to the <a href="http://mzmine.github.io/">MZmine pipeline</a>,
detecting masses refers to setting a signal intensity threshold that MS/MS ions
has to meet to be kept in the data, while signals that do not meet the defined
threshold are removed. This function can also normalize the spectra ion
intensity to percentage based on the base peak. This is a filtering step
that is based on percentage of the base peak (most intense ion).</p>
<p>The three required arguments are:</p>
<ul>
<li><strong>spec:</strong> a data frame containing the MS/MS spectra.</li>
<li><strong>normalize:</strong> a boolean indicating if the MS/MS spectra is normalized by the base peak before proceeding to filter out low intensity signals (normalize = TRUE), if normalize = FALSE the user has to provide the minimum ion count.</li>
<li><strong>min_int:</strong> an integer referring to the minimum ion intensity. The value of <em>min_int</em> has to be in line if user decides to normalize the spectra or not. If the spectra is normalized, the <em>min_intensity</em> value is in percentage, otherwise the <em>min_intensity</em> value is expressed in ion count units.</li>
</ul>
<p>By default, the normalization is set to <code>TRUE</code> and the minimum intensity is
set to 1% to remove background noise.</p>
<pre class="r"><code>ProcA2_detected &lt;- detect_mass(ProcA2_extracted, normalize = TRUE, min_int = 1)</code></pre>
<p>We can see now the range of m/z values and the maximum value is 576.1221 m/z.</p>
<pre class="r"><code>range(ProcA2_detected$mz)
#&gt; [1] 125.0243 576.1221</code></pre>
</div>
<div id="msms-spectra-plot" class="section level2">
<h2>MS/MS spectra plot</h2>
<p>We can proceed to plot the filtered MS/MS spectra with <code>plot_MS2spectra()</code>
function. This is a ggplot2 based function; the blue diamond refers to the precursor ion.</p>
<p>If we take a look to the previous MS/MS plot, there is less background noise
in this MS/MS spectra because the low intensity ions have been removed.</p>
<pre class="r"><code>plot_MS2spectra(ProcA2_detected)
#&gt; Warning: `position_stack()` requires non-overlapping x intervals</code></pre>
<p><img src="unnamed-chunk-12-1.png" width="100%" /></p>
</div>
<div id="exporting-msms-spectra" class="section level2">
<h2>Exporting MS/MS spectra</h2>
<div id="nist-.msp-format" class="section level3">
<h3>NIST .msp format</h3>
<p>Finally after extracting the MS/MS spectra and removing background noise, we
can proceed to export the MS/MS in a NIST .msp format.</p>
<p>For this task, we need extra information about the compound, such as
SMILES, COLLISIONENERGY, etc. You can find the minimum required information
by accessing the <code>write_msp()</code> function help by running the command
<code>?write_msp</code>.</p>
<p>An example of this table can be found at:</p>
<pre class="r"><code># Reading the metadata
metadata_file &lt;- system.file(&quot;extdata&quot;,
  &quot;msp_metadata.csv&quot;,
  package = &quot;MS2extract&quot;
)

metadata &lt;- read.csv(metadata_file)
dplyr::glimpse(metadata)
#&gt; Rows: 1
#&gt; Columns: 8
#&gt; $ NAME            &lt;chr&gt; &quot;Procyanidin A2&quot;
#&gt; $ PRECURSORTYPE   &lt;chr&gt; &quot;[M-H]-&quot;
#&gt; $ FORMULA         &lt;chr&gt; &quot;C30H24O12&quot;
#&gt; $ INCHIKEY        &lt;chr&gt; &quot;NSEWTSAADLNHNH-LSBOWGMISA-N&quot;
#&gt; $ SMILES          &lt;chr&gt; &quot;C1C(C(OC2=C1C(=CC3=C2C4C(C(O3)(OC5=CC(=CC(=C45)O)O)C6…
#&gt; $ IONMODE         &lt;chr&gt; &quot;Negative&quot;
#&gt; $ INSTRUMENTTYPE  &lt;chr&gt; &quot;LC-ESI-QTOF&quot;
#&gt; $ COLLISIONENERGY &lt;chr&gt; &quot;20 eV&quot;</code></pre>
<p>The three arguments for this function are:</p>
<ul>
<li><strong>spec:</strong> a data frame containing the extracted MS/MS spectra</li>
<li><strong>spec_metadata:</strong> a data frame containing the values to be including in the resulting .msp file</li>
<li><strong>msp_name:</strong> a string with the name of the msp file not containing (.msp) extension</li>
</ul>
<pre class="r"><code>write_msp(
  spec = ProcA2_detected,
  spec_metadata = metadata,
  msp_name = &quot;Procyanidin_A2&quot;
)</code></pre>
<p>After writing the msp file, you will see the following file content:</p>
<pre><code>#&gt; • Filtering MS/MS scans for 20 CE
#&gt; NAME: Procyanidin A2
#&gt; PRECURSORMZ: 575.11957
#&gt; PRECURSORTYPE: [M-H]-
#&gt; FORMULA: C30H24O12
#&gt; RETENTIONTIME: 2.844
#&gt; IONMODE: Negative
#&gt; COMMENT: Spectra extracted with MS2extract R package
#&gt; INCHIKEY: NSEWTSAADLNHNH-LSBOWGMISA-N
#&gt; SMILES: C1C(C(OC2=C1C(=CC3=C2C4C(C(O3)(OC5=CC(=CC(=C45)O)O)C6=CC(=C(C=C6)O)O)O)O)C7=CC(=C(C=C7)O)O)O
#&gt; CCS: 
#&gt; COLLISIONENERGY: 20 eV
#&gt; INSTRUMENTTYPE: LC-ESI-QTOF
#&gt; Num Peaks: 38
#&gt; 125.02431 10
#&gt; 137.02441 3
#&gt; 161.02449 2
#&gt; 163.00355 3
#&gt; 165.01881 2
#&gt; 217.04996 2
#&gt; 241.05002 2
#&gt; 245.04547 2
#&gt; 245.0817 2
#&gt; 257.0451 2
#&gt; 285.04063 62
#&gt; 286.04387 4
#&gt; 287.05579 7
#&gt; 289.0718 48
#&gt; 290.07495 3
#&gt; 297.03993 4
#&gt; 307.06114 2
#&gt; 313.03573 2
#&gt; 327.05044 7
#&gt; 407.07693 16
#&gt; 408.08161 2
#&gt; 411.07227 9
#&gt; 423.07231 53
#&gt; 424.07537 5
#&gt; 435.07138 3
#&gt; 447.07296 5
#&gt; 449.08799 51
#&gt; 450.09044 6
#&gt; 452.07453 15
#&gt; 453.08155 7
#&gt; 471.1086 2
#&gt; 513.11796 2
#&gt; 531.13006 2
#&gt; 539.09834 22
#&gt; 540.10156 3
#&gt; 557.10809 5
#&gt; 575.11968 100
#&gt; 576.12208 13</code></pre>
</div>
</div>
</div>
<div id="session-info" class="section level1">
<h1>Session info</h1>
<pre class="r"><code>sessionInfo()
#&gt; R version 4.3.1 (2023-06-16)
#&gt; Platform: aarch64-apple-darwin20 (64-bit)
#&gt; Running under: macOS Ventura 13.5.1
#&gt; 
#&gt; Matrix products: default
#&gt; BLAS:   /Library/Frameworks/R.framework/Versions/4.3-arm64/Resources/lib/libRblas.0.dylib 
#&gt; LAPACK: /Library/Frameworks/R.framework/Versions/4.3-arm64/Resources/lib/libRlapack.dylib;  LAPACK version 3.11.0
#&gt; 
#&gt; locale:
#&gt; [1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8
#&gt; 
#&gt; time zone: America/New_York
#&gt; tzcode source: internal
#&gt; 
#&gt; attached base packages:
#&gt; [1] stats     graphics  grDevices utils     datasets  methods   base     
#&gt; 
#&gt; other attached packages:
#&gt; [1] MS2extract_0.01.0
#&gt; 
#&gt; loaded via a namespace (and not attached):
#&gt;  [1] tidyselect_1.2.0      farver_2.1.1          dplyr_1.1.3          
#&gt;  [4] fastmap_1.1.1         blogdown_1.18         XML_3.99-0.14        
#&gt;  [7] digest_0.6.33         lifecycle_1.0.3       cluster_2.1.4        
#&gt; [10] ProtGenerics_1.32.0   magrittr_2.0.3        compiler_4.3.1       
#&gt; [13] rlang_1.1.1           sass_0.4.7            tools_4.3.1          
#&gt; [16] utf8_1.2.3            yaml_2.3.7            knitr_1.43           
#&gt; [19] ggsignif_0.6.4        labeling_0.4.3        plyr_1.8.8           
#&gt; [22] abind_1.4-5           BiocParallel_1.34.2   withr_2.5.0          
#&gt; [25] purrr_1.0.2           BiocGenerics_0.46.0   grid_4.3.1           
#&gt; [28] stats4_4.3.1          preprocessCore_1.62.1 fansi_1.0.4          
#&gt; [31] ggpubr_0.6.0          colorspace_2.1-0      ggplot2_3.4.3        
#&gt; [34] scales_1.2.1          iterators_1.0.14      MASS_7.3-60          
#&gt; [37] cli_3.6.1             mzR_2.34.1            rmarkdown_2.23       
#&gt; [40] generics_0.1.3        Rdisop_1.60.0         rstudioapi_0.15.0    
#&gt; [43] tzdb_0.4.0            readxl_1.4.3          ncdf4_1.21           
#&gt; [46] cachem_1.0.8          affy_1.78.2           zlibbioc_1.46.0      
#&gt; [49] parallel_4.3.1        impute_1.74.1         cellranger_1.1.0     
#&gt; [52] BiocManager_1.30.22   vsn_3.68.0            vctrs_0.6.3          
#&gt; [55] jsonlite_1.8.7        carData_3.0-5         bookdown_0.34        
#&gt; [58] car_3.1-2             hms_1.1.3             IRanges_2.34.1       
#&gt; [61] S4Vectors_0.38.1      ggrepel_0.9.3         MALDIquant_1.22.1    
#&gt; [64] rstatix_0.7.2         clue_0.3-64           foreach_1.5.2        
#&gt; [67] limma_3.56.2          jquerylib_0.1.4       tidyr_1.3.0          
#&gt; [70] affyio_1.70.0         glue_1.6.2            MSnbase_2.26.0       
#&gt; [73] codetools_0.2-19      cowplot_1.1.1         gtable_0.3.4         
#&gt; [76] OrgMassSpecR_0.5-3    mzID_1.38.0           munsell_0.5.0        
#&gt; [79] tibble_3.2.1          pillar_1.9.0          pcaMethods_1.92.0    
#&gt; [82] htmltools_0.5.6       R6_2.5.1              Rdpack_2.5           
#&gt; [85] doParallel_1.0.17     evaluate_0.21         lattice_0.21-8       
#&gt; [88] Biobase_2.60.0        readr_2.1.4           highr_0.10           
#&gt; [91] rbibutils_2.2.15      backports_1.4.1       broom_1.0.5          
#&gt; [94] bslib_0.5.1           Rcpp_1.0.11           xfun_0.40            
#&gt; [97] MsCoreUtils_1.12.0    pkgconfig_2.0.3</code></pre>
</div>
