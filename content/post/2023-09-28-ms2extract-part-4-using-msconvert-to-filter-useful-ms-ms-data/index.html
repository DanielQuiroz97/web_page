---
title: MS2extract Part 4 - Using MSconvert to filter useful MS/MS data
author: Daniel Quiroz & Jessica Cooperstone
date: '2023-09-28'
slug: [MS2extract]
categories:
  - LC-MS
tags:
  - MS2extract
  - MS/MS libraries
  - LC-MS
  - DataViz
subtitle: 'Using MSconvert to convert and crop MS/MS data'
description: 'In this post we are going to explain how you can use MSconvert to crop specific parts of your MS/MS run to preserve only the MS/MS scans of your reference material'
image: ''
---



<div id="goal-of-this-document" class="section level1">
<h1>Goal of this document</h1>
<p>In this document, you will find more information describing multiple
scenarios about how to use
<a href="https://link.springer.com/protocol/10.1007/978-1-4939-6747-6_23">MSconvert</a>
in order to transform your raw MS/MS data to .mzMl or mzXML format in
order to be imported with
<a href="https://github.com/CooperstoneLab/MS2extract">MS2extract</a>.</p>
<p>Here, we will explore three different scenarios that you might find
useful depending on your MS/MS data collection approach. The explained
scenarios are:</p>
<ul>
<li>Converting to .mzML the entire run,</li>
<li>Trimming for specific retention time, and</li>
<li>Extracting MS/MS scans of multiples CE from the same run</li>
</ul>
<p>*Note:* this is the fourth vignette for this package and if you find some
terms unclear, or are not familiar with the concepts, please visit the
previous vignettes (<a href="https://cooperstonelab.github.io/MS2extract/articles/1_introduction.html">Package
introduction</a>,
<a href="https://cooperstonelab.github.io/MS2extract/articles/2_using_batch_extract.html">Batch mode
introduction</a>,
and <a href="https://cooperstonelab.github.io/MS2extract/articles/4_import_mzml_explanation.html">Importing MS/MS
data</a>).</p>
</div>
<div id="concept-of-ms2extract" class="section level1">
<h1>Concept of MS2extract</h1>
<p>Before moving to the hands-on application, we would like to clarify some
definitions about this software that could help improve the explanation
of the following case examples.</p>
<p>The <a href="https://github.com/CooperstoneLab/MS2extract">MS2extract</a> concept
can be summarized in two main subsequent tasks; (1) look for scans with
a specific precursor ion <em>m/z</em>, and (2) extract the MS/MS fragmentation
pattern.</p>
<p>Therefore, your data file (<em>.mzML</em>) must contain at least one MS/MS scan
with the specified precursor ion to extract the MS/MS data.</p>
</div>
<div id="case-1-converting-to-.mzml-the-entire-run" class="section level1">
<h1>Case 1: Converting to .mzML the entire run</h1>
<p>In this case, although is not the optimal approach, it will get the job
done, if your data meets a requirement.</p>
<blockquote>
<p>Requirement: the precursor ion of your standard has to produce the
most intense MS/MS signal in the entire run.</p>
</blockquote>
<blockquote>
<p>Requirement: If you have isomers in the same run, but they elute at
different retention times, you must provide different retention times
windows.</p>
</blockquote>
<p>Since <a href="https://github.com/CooperstoneLab/MS2extract">MS2extract</a>
searches for the most intense MS/MS scan, you can only have one
metabolite per run if you do not provide different retention time
windows.</p>
<p>Since this package only works with MS/MS data, we are only going to keep
scans with MS/MS data. Therefore, the MSconvert filters are:</p>
<table>
<thead>
<tr class="header">
<th>Filter</th>
<th>Parameters</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>peakPicking</td>
<td>vendor msLevel=2-2</td>
</tr>
<tr class="even">
<td>msLevel</td>
<td>2-2</td>
</tr>
</tbody>
</table>
<p><img src="case_1.png" /></p>
</div>
<div id="case-2-trimming-for-specific-retention-time" class="section level1">
<h1>Case 2: Trimming for specific retention time</h1>
<p>This is probably the most efficient approach to convert your raw data
files, since you already know the retention time of your metabolite.
Then, you can use the scanTime filter. Remember, scan time has to be
converted to seconds, not minutes.</p>
<p>We used this approach to build the entire
<a href="https://github.com/CooperstoneLab/PhenolicsDB">PhenolicsDB</a> repository,
where you can find the retention time window for each metabolite.</p>
<table>
<thead>
<tr class="header">
<th>Filter</th>
<th>Parameters</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>peakPicking</td>
<td>vendor msLevel=2-2</td>
</tr>
<tr class="even">
<td>msLevel</td>
<td>2-2</td>
</tr>
<tr class="odd">
<td>scanTime</td>
<td>[min rt, max rt]</td>
</tr>
</tbody>
</table>
<p>In the following example, we are using the procyanidin B2 standard data,
with a elution time of 138 (s), and the minimum and maximum retention
time are 133 and 145 (s), respectively. Therefore, the MSconvert task
will look like the following image.</p>
<p><img src="mzml_trimming.png" /></p>
</div>
<div id="case-3-multiple-ce-in-the-same-run" class="section level1">
<h1>Case 3: Multiple CE in the same run</h1>
<p>Unfortunately, the current version of
<a href="https://github.com/CooperstoneLab/MS2extract">MS2extract</a> does not
support importing MS/MS data with different CE in the same run. Then,
the user will need to manually separate different CE in different files
as it is explained below.</p>
<p>For this example, we are going to use guaijaverin (quercetin
3-arabinopyranoside) to exemplify this case. In the following image, you
can see the EIC of this metabolite in negative polarity. Then, the first
MS/MS spectra refers to the fragmentation pattern at 60 eV, while the
second MS/MS spectra refers to 80 eV.</p>
<p><img src="Case_3A.png" /></p>
<p>Now, in order to separate scans in MSconvert we need to specify one CE
at a time. Here, you can see that we are using the <em>collisionEnergy</em>
filter to subset MS/MS scans with a CE of 60 eV. Then, the filters for
this metabolite will be:</p>
<table>
<thead>
<tr class="header">
<th>Filter</th>
<th>Parameters</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>peakPicking</td>
<td>vendor msLevel=2-2</td>
</tr>
<tr class="even">
<td>msLevel</td>
<td>2-2</td>
</tr>
<tr class="odd">
<td>scanTime</td>
<td>[min rt, max rt]</td>
</tr>
<tr class="even">
<td>collisionEnergy</td>
<td>low=60 high=60</td>
</tr>
</tbody>
</table>
<p><img src="Case_3b.png" /></p>
<p>Once you convert this raw data file to mzML at 60 eV, you can change the
collision energy to a different value and then convert for the rest of
available CE values. The same principle can be applied for MS/MS data
with multiple polarities in the same run.</p>
<blockquote>
<p>Note: remember to change the .mzml file name as it will overwrite the
file with the new CE value.</p>
</blockquote>
</div>
