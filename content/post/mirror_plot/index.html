---
title: Making publication-ready MS/MS mirror plots
author: Daniel Quiroz, Jefferson Pastuña, and Jessica Cooperstone
date: '2023-09-29'
slug: [MS/MS]
categories:
  - LC-MS
tags:
  - MS2extract
  - MS/MS libraries
  - LC-MS
  - DataViz
subtitle: 'A step-by-step tutorial for creating MS/MS mirror plots in R'
description: 'This post shows how to create beatiful mirror plots to compare experimental vs reference MS/MS fragementation pattern with a slight focus on metabolomics.'
image: '/img/niagara.jpg'
editor_options: 
  markdown: 
    wrap: 72
---



<div id="goal-of-this-document" class="section level2">
<h2>Goal of this document</h2>
<p>This post is a step-by-step tutorial about how to create publication-ready
mirror plots to show the similarities, or differences between an experimental
and reference MS/MS spectra.</p>
<p>This post will assume you have two MS/MS spectra you’d like to compare.
This will most often be an experimental spectrum you’ve collected,
and a reference spectrum from a standard or database.
By the end of this post, you will be familiar with data visualization
techniques needed to create and customize MS/MS mirror plots.
Some examples of final plots are below:</p>
<p><strong>Experimental vs. Analytical standard spectrum</strong></p>
<div style="text-align: center;">
<p><img src="img/nictoflorin.png" height="100%" width="100%" class="center"></p>
</div>
<p><strong>Experimental vs. literature spectrum</strong></p>
<div style="text-align: center;">
<p><img src="img/isoschaftoside.png" height="100%" width="100%" class="center"></p>
</div>
<p><strong>Disclaimer</strong></p>
<blockquote>
<p>This post will not cover metabolite identification approaches.</p>
</blockquote>
<p>There is a lot of material about approaches for metabolite identification
, and we encourage to visit these references if you are not familiar with
this topic.</p>
<p>Here is the list of few places that you can visit to get familiar with
metabolite ID.</p>
<ul>
<li><a href="https://ccms-ucsd.github.io/GNPSDocumentation/">GNPS documentation</a></li>
<li><a href="https://www.youtube.com/@dorresteinlab4428">GNPS tutorial videos</a></li>
<li><a href="http://prime.psc.riken.jp/compms/msfinder/main.html">MS-DIAL/MS-FINDER tutorials</a></li>
</ul>
</div>
<div id="introduction" class="section level2">
<h2>Introduction</h2>
<p>Metabolite identification from untargeted mass spectrometry
based metabolomics data can be conducted by using different platforms and
approaches. The gold standard is comparing the m/z and retention time of
a peak in your sample to that of an authentic standard. However, given that
often this type of matching is impossible or impractical, another
commonly usedapproach is comparing the MS/MS fragmentation pattern of your
experimental data against a reference spectrum. This reference spectrum
could come from difference sources. In the best case scenario, you would
have a reference material (analytical standard), and you will be able to
collect MS/MS data of this material. Another option is using public or
licensed MS/MS libraries.</p>
<p>Although the concept of comparing the fragmentation patterns of the experimental
spectrum against a reference spectra sounds fairly easy, this task is not trivial, as
mathematical methods to quantify the similarity between two vectors or matrices are
needed. There is a great read for in this topic that you can direct your
attention if you are interested in this topic
<a href="https://link.springer.com/article/10.1007/s11306-022-01963-y">Niek F. de Jonge et al. 2022</a>.</p>
<p>Once you have successfully processed your MS/MS data and have found or
collected an MS/MS spectrum that you think matches your compound,
you can make a figure that compares those spectra together so
others can evaluate their similarity.
The first step though is to gather your MS/MS data.</p>
</div>
<div id="gathering-msms-data" class="section level2">
<h2>Gathering MS/MS data</h2>
<p>This section aims to briefly explain how to extract MS/MS data from your raw
files to create a table of <em>m</em>/<em>z</em> values and ion intensities to create
the mirror plot. If you already have these data, you can jump to the
next section.</p>
<p>Here, I will explain how we extracted the tables containing <em>m</em>/<em>z</em> values
and ion intensities for making the mirror plots.
If you want to replicate these plots, you can download these tables.</p>
<ul>
<li><a href="https://gitlab.com/DanielQuiroz97/danielquiroz97.gitlab.io/-/blob/master/content/post/2023-09-29-making-ms-ms-mirror-plots-to-compare-experimetnal-vs-reference-spectra/data/Nictoflorin.xlsx?ref_type=heads">Nictoflorin example data</a></li>
<li><a href="https://gitlab.com/DanielQuiroz97/danielquiroz97.gitlab.io/-/blob/master/content/post/2023-09-29-making-ms-ms-mirror-plots-to-compare-experimetnal-vs-reference-spectra/data/Isoschaftoside.xlsx?ref_type=heads">Isoschaftoside example data</a></li>
</ul>
<div id="experimental" class="section level3">
<h3>Experimental</h3>
<p>The experimental MS/MS spectrum comes from you data. Either form the raw MS/MS
files, or from the deconvoluted MS/MS peaks.</p>
<p>Here is an example how we used <a href="http://mzmine.github.io">MZmine3</a> to extract
the <em>m/z</em> and ion intensity table from the nictoflorin standard. If you
want to download the raw nictolorin <em>.mzml</em> file, you can find it in the
<a href="https://cooperstonelab.github.io/PhenolicsDB/">PhenolicsDB</a> at this
<a href="https://github.com/CooperstoneLab/PhenolicsDB/blob/main/inst/extdata/QTOF_6545/Neg/40/40eV_kaempferol3rutinoside_neg_62.mzML">link</a></p>
<p>Once you load your data in <a href="http://mzmine.github.io">MZmine3</a>, you need to select the
MS/MS scan that has the desired MS/MS data. Then, in the MS/MS plot panel,
you can right-click and export this spectrum to an excel file that we are going
to use later.</p>
<p>You can use the same process to extract the data from your experimental files.</p>
<div style="text-align: center;">
<p><img src="img/mzmine.png" height="100%" width="100%" class="center"></p>
</div>
</div>
<div id="literature-spectra" class="section level3">
<h3>Literature spectra</h3>
<p>You need to find the repository or have access to the MS/MS ion
peak list of the literature spectrum. In this case, our literature spectrum matches
to isoschaftoside from MassBank, with the accession number
<a href="https://massbank.eu/MassBank/RecordDisplay?id=MSBNK-Fiocruz-FIO00727">MSBNK-Fiocruz-FIO00727</a>.</p>
<p>Here, you can simply copy the m/z and intensity values to an excel file, or
any file as long as you have these to pieces of information.</p>
<div style="text-align: center;">
<p><img src="img/massbank.png" height="100%" width="100%" class="center"></p>
</div>
</div>
</div>
<div id="data-wrangling" class="section level2">
<h2>Data Wrangling</h2>
<div id="loading-libraries" class="section level3">
<h3>Loading libraries</h3>
<p>Here, we are going to list and load all R libraries needed to create the mirror plots.</p>
<pre class="r"><code>library(tidyverse) # Data wrangling and plotting
library(readxl) # Importing excel data
library(ggrepel) # Overlapping labels
library(cowplot) # Inserting chemical structure
library(magick) # Importing images in tiff format</code></pre>
</div>
<div id="importing-msms-data" class="section level3">
<h3>Importing MS/MS data</h3>
<p>As we mentioned earlier, we are going to work with two examples, the mirror plot
for isoschaftoside and in a next post we will cover the example of the
nictoflorin mirror plot. While the mirror plot of isoschaftoside
reflects example of a match to a literature spectrum, the nictoflorin mirror
plot is an example of a math to an analytical standard spectrum.</p>
<p>We are going to show how to create the mirror plot of isoschaftoside first, as it
does not have ions with super close <em>m/z</em> values that we need to label, which
requires extra work.</p>
<p>Both provided excel files has three columns:</p>
<ul>
<li><strong>mz</strong>: <em>m/z</em> values</li>
<li><strong>intensity</strong>: ion signal intensity</li>
<li><strong>Group</strong>: refers if the signal is from the experimental or reference spectrum</li>
</ul>
<pre class="r"><code># Isoschaftoside data
isoschaftoside_data &lt;- readxl::read_excel(&quot;data/Isoschaftoside.xlsx&quot;, sheet = 1)
glimpse(isoschaftoside_data[seq(3), ])
#&gt; Rows: 3
#&gt; Columns: 3
#&gt; $ mz        &lt;chr&gt; &quot;  191.034100 &quot;, &quot;  221.045900 &quot;, &quot;  233.045100 &quot;
#&gt; $ intensity &lt;chr&gt; &quot;326.000000 &quot;, &quot;277.000000 &quot;, &quot;252.000000 &quot;
#&gt; $ Group     &lt;chr&gt; &quot;Standard&quot;, &quot;Standard&quot;, &quot;Standard&quot;</code></pre>
<p>We can note that the <code>mz</code> columns was imported as character and not as number.
It is because there are empty characters that prevents R to import this colum
as character. The solution is to use the <code>parse_number()</code> function.</p>
<pre class="r"><code>isoschaftoside_data &lt;- isoschaftoside_data %&gt;% 
  mutate(intensity = parse_number(intensity), # Correcting numeric values
         mz = parse_number(mz))

glimpse(isoschaftoside_data)
#&gt; Rows: 660
#&gt; Columns: 3
#&gt; $ mz        &lt;dbl&gt; 191.0341, 221.0459, 233.0451, 282.0529, 283.0603, 295.0601, …
#&gt; $ intensity &lt;dbl&gt; 326, 277, 252, 324, 305, 368, 1454, 2380, 389, 397, 308, 150…
#&gt; $ Group     &lt;chr&gt; &quot;Standard&quot;, &quot;Standard&quot;, &quot;Standard&quot;, &quot;Standard&quot;, &quot;Standard&quot;, …</code></pre>
<p>We can see the <code>mz</code> and <code>intensity</code> column are properly format as <code>dbl</code>,
or a numeric variable. Now, we are going to create two groups in this dataset,
as we have the ions from the experimental and reference spectrum.</p>
<pre class="r"><code>isoschaftoside_data &lt;- isoschaftoside_data %&gt;% 
  group_by(Group) # Grouping by standard and sample

glimpse(isoschaftoside_data)
#&gt; Rows: 660
#&gt; Columns: 3
#&gt; Groups: Group [2]
#&gt; $ mz        &lt;dbl&gt; 191.0341, 221.0459, 233.0451, 282.0529, 283.0603, 295.0601, …
#&gt; $ intensity &lt;dbl&gt; 326, 277, 252, 324, 305, 368, 1454, 2380, 389, 397, 308, 150…
#&gt; $ Group     &lt;chr&gt; &quot;Standard&quot;, &quot;Standard&quot;, &quot;Standard&quot;, &quot;Standard&quot;, &quot;Standard&quot;, …</code></pre>
<p>At the top of the output we can see that two groups were created, the
<code>standard</code> and <code>sample</code> group.</p>
</div>
<div id="relative-abundance-calculation" class="section level3">
<h3>Relative abundance calculation</h3>
<p>Since the ion abundance of both spectra are in ion counts
(i.e., raw abundance), and the scale of the experimental and
standard data are different, we need to calculate the relative abundance,
that can be calculated by dividing each intensity by the max intensity,
times 100. This will allow us to compare spectra that have different
raw intensities.</p>
<p>We can implement this using the code below:</p>
<pre class="r"><code>
isoschaftoside_data &lt;- isoschaftoside_data %&gt;% 
   mutate(Rel_int = intensity/max(intensity)*100) # Relative abundance calc

# Printing ion abundance greater than 50%
isoschaftoside_data %&gt;% filter(Rel_int &gt; 50) 
#&gt; # A tibble: 4 × 4
#&gt; # Groups:   Group [2]
#&gt;      mz intensity Group    Rel_int
#&gt;   &lt;dbl&gt;     &lt;dbl&gt; &lt;chr&gt;      &lt;dbl&gt;
#&gt; 1  353.     25145 Standard   100  
#&gt; 2  383.     22453 Standard    89.3
#&gt; 3  353.     19000 Sample     100  
#&gt; 4  383.     15000 Sample      78.9</code></pre>
<p>For example, this table shows the ions that has a greater relative abundance
than 50%.</p>
<p>We can also print how many ions are found in both the sample and the standard.</p>
<pre class="r"><code>isoschaftoside_data %&gt;% count() # Counting number ions per group
#&gt; # A tibble: 2 × 2
#&gt; # Groups:   Group [2]
#&gt;   Group        n
#&gt;   &lt;chr&gt;    &lt;int&gt;
#&gt; 1 Sample     605
#&gt; 2 Standard    55</code></pre>
<p>Until this point, the sample (experimental) and the standard peak list have 605
and 55 ions, respectively. The larger number of ions in sample is attributed to
the fact that this spectra was not processed to remove low intensity signals.</p>
</div>
<div id="filtering-low-abundance-ions" class="section level3">
<h3>Filtering low abundance ions</h3>
<p>We can process our data to remove ions with low abundance. In our case,
we are going to remove signals below 1.2% intensity,
and ions less than 280 <em>m</em>/<em>z</em>. The first
criterion aims to remove low signal ions that are the product of
background noise. We are also removing ions less than 280 <em>m</em>/<em>z</em> as reference
spectra do not show any signal below this <em>m</em>/<em>z</em> value.</p>
<p>Note from authors: With programming you can handle/manipulate your data in an
unlimited ways, so please be honest and report precisely how you process your
data.</p>
<pre class="r"><code>isoschaftoside_data &lt;- isoschaftoside_data %&gt;% 
  filter(Rel_int &gt; 1.2) %&gt;%  # Removing signal less than 1.2% intensity
  filter(mz &gt; 280) # Removing ions below 280 m/z
  
isoschaftoside_data %&gt;% count()
#&gt; # A tibble: 2 × 2
#&gt; # Groups:   Group [2]
#&gt;   Group        n
#&gt;   &lt;chr&gt;    &lt;int&gt;
#&gt; 1 Sample      42
#&gt; 2 Standard    50</code></pre>
<p>With the final spectra processing approach, we had a final number of ions of 42 and 50
ions for the sample and standard spectra, respectively.</p>
</div>
<div id="negative-intensities-for-the-reference-spectrum" class="section level3">
<h3>Negative intensities for the reference spectrum</h3>
<p>If we recall the peak table, both, the experimental and the reference spectra have
positive values. The core concept of mirror plot is that the reference
spectra has negative intensity values, which will be plotted as the mirror plot
of the experimental spectrum.</p>
<pre class="r"><code>isoschaftoside_data %&gt;% filter(Rel_int &gt; 50) 
#&gt; # A tibble: 4 × 4
#&gt; # Groups:   Group [2]
#&gt;      mz intensity Group    Rel_int
#&gt;   &lt;dbl&gt;     &lt;dbl&gt; &lt;chr&gt;      &lt;dbl&gt;
#&gt; 1  353.     25145 Standard   100  
#&gt; 2  383.     22453 Standard    89.3
#&gt; 3  353.     19000 Sample     100  
#&gt; 4  383.     15000 Sample      78.9</code></pre>
<p>Therefore, we need to make the intensity values of the reference spectrum
to be negative.</p>
<pre class="r"><code># Changing standard intensity values to negative
# We eval the match of signals to belong to the standard groups, and
# multiply the intensity value time -1

isoschaftoside_data &lt;- isoschaftoside_data %&gt;% ungroup %&gt;% 
  mutate(Rel_int = ifelse(Group %in% &quot;Standard&quot;,
                          Rel_int*-1, Rel_int))

isoschaftoside_data %&gt;% filter(Rel_int &gt; 50 | Rel_int &lt; -50) 
#&gt; # A tibble: 4 × 4
#&gt;      mz intensity Group    Rel_int
#&gt;   &lt;dbl&gt;     &lt;dbl&gt; &lt;chr&gt;      &lt;dbl&gt;
#&gt; 1  353.     25145 Standard  -100  
#&gt; 2  383.     22453 Standard   -89.3
#&gt; 3  353.     19000 Sample     100  
#&gt; 4  383.     15000 Sample      78.9</code></pre>
<p>Now, we can see that the intensity values from the standard are negative while
the sample remain positive.</p>
<p>At this point, we finally have the data ready for plotting.</p>
</div>
</div>
<div id="plotting-a-mirror-plot" class="section level2">
<h2>Plotting a mirror plot</h2>
<div id="backbone-plot" class="section level3">
<h3>Backbone plot</h3>
<p>The backbone of a mirror plot is a barchart. Therefore, we are going to use
this geometry for this purpose <code>geom_col()</code>.</p>
<pre class="r"><code>isos_mirror &lt;- isoschaftoside_data %&gt;% 
  ggplot(aes(mz, Rel_int,  fill = Group)) +
  geom_col(width = 0.6) 

isos_mirror</code></pre>
<p><img src="unnamed-chunk-11-1.png" width="672" /></p>
<p>This is the backbone of our plot, now we are going to work to make it look nicer.</p>
</div>
<div id="changing-group-colors" class="section level3">
<h3>Changing group colors</h3>
<p>Based on the GNPS mirror plot colors, we are going to change to a black
color for the experimental MS/MS, while the reference spectra will be green.</p>
<pre class="r"><code>isos_mirror &lt;- isos_mirror + 
  scale_fill_manual(values = c(Sample = &quot;#000000&quot;, Standard = &quot;#4B9C15&quot;))
isos_mirror</code></pre>
<p><img src="unnamed-chunk-12-1.png" width="672" /></p>
</div>
<div id="background-color-and-grids" class="section level3">
<h3>Background color and grids</h3>
<p>The gray background is the default color in ggplot, but we can change this
color to white, remove the grids, and remove the repetitive legend.</p>
<pre class="r"><code>isos_mirror &lt;- isos_mirror + 
  theme_light() + # Using a white background theme
  theme(legend.position = &quot;none&quot;, # Removing legend
        panel.grid.major = element_blank(),  # Removing grins
        panel.grid.minor = element_blank()) 

isos_mirror</code></pre>
<p><img src="unnamed-chunk-13-1.png" width="672" /></p>
<p>We have a cleaner plot, but the axis labels need to be altered, and a title
added.</p>
</div>
<div id="changing-labels" class="section level3">
<h3>Changing labels</h3>
<pre class="r"><code>isos_mirror &lt;- isos_mirror + 
  labs(x = &quot;m/z&quot;, # X axis label
       y = &quot;Relative intensity (%)&quot;, # Y axis label
       title = &quot;Isoschaftoside [M-H]-&quot;)# Title label
isos_mirror</code></pre>
<p><img src="unnamed-chunk-14-1.png" width="672" /></p>
</div>
<div id="axis-limits" class="section level3">
<h3>Axis limits</h3>
<p>The mirror plot is looking more like to the mirror plot we showed at the top
of this post. Next, lets set the <em>x</em> and <em>y</em> axis limits to make room for
the additional labels.</p>
<pre class="r"><code># Text annotation coordinates
annotation_text &lt;- data.frame(
  x = c(170, 160),
  y = c(140, -140),
  label = c(&quot;Experimental spectrum 40 eV&quot;,
            &quot;Literature spectrum 50 eV&quot;),
  Group = c(&quot;Sample&quot;, &quot;Standard&quot;)
)

isos_mirror &lt;- isos_mirror + 
   ylim(c(-140, 140)) + # Y axis limit
  xlim(c(100, 560)) + # X axis limit
  geom_text(data = annotation_text, 
            aes(x = x, y = y, label = label)) 
isos_mirror</code></pre>
<p><img src="unnamed-chunk-15-1.png" width="672" /></p>
</div>
<div id="adding-labels" class="section level3">
<h3>Adding labels</h3>
<p>Next, we can proceed to add the labels for the m/z for the most intense ions.
Here, you can take multiple approaches as what ions you want to label.
For example, in the case of the isoschaftoside mirror plot, we are going to
use only the ions with a relative abundance greater than 10%. On the other
hand, in the case of nictoflorin, we are going to use the same 10% cutoff,
and we are going to add the precursor ion.e the same 10% cutoff, but we are
going to add the precursor ion.</p>
<div id="experimental-spectrum-lables" class="section level4">
<h4>Experimental spectrum lables</h4>
<pre class="r"><code># Filtering ions in experimental spectrum
isos_labs_pos &lt;- isoschaftoside_data %&gt;%
  filter( Rel_int &gt; 10 ) 

isos_mirror &lt;- isos_mirror + 
  geom_text(data = isos_labs_pos,
            aes(label = abs(round(mz,3)), # Round label to 3 decimal digits
                y = Rel_int),
            size = 3, # Label size 
            angle = 90, # Rotate label 90 degree
            hjust = -0.1)  # Place after the max intensity

isos_mirror</code></pre>
<p><img src="unnamed-chunk-16-1.png" width="672" /></p>
</div>
<div id="reference-spectra-lables" class="section level4">
<h4>Reference spectra lables</h4>
<p>Finally, we need to add the labels in the reference spectrum.</p>
<pre class="r"><code># Filtering ions in the reference spectrum
isos_labs_neg &lt;- isoschaftoside_data %&gt;%
  filter( Rel_int &lt; -10  ) 

isos_mirror &lt;- isos_mirror +
  geom_text(data = isos_labs_neg, aes(label = abs(round(mz,3)),
                                      y = Rel_int),
            size = 3, angle = 90, hjust = 1, vjust = 1, color = &quot;#4B9C15&quot;)
isos_mirror</code></pre>
<p><img src="unnamed-chunk-17-1.png" width="672" /></p>
<p>At this point, you almost have a final mirror plot. The final (optional)
step is to add the chemical structure of the metabolite.</p>
</div>
</div>
<div id="inserting-the-chemical-structure" class="section level3">
<h3>Inserting the chemical structure</h3>
<p>You have couple of options for this task. By far, the easiest is to
create or get the chemical structure of the metabolite in a different
software (e.g., ChemDraw) and use an image processing software to
join the mirror plot and the chemical structure in a final figure.</p>
<p>There are some online options to create/draw your chemical structure such as
<a href="https://molview.org">MolView</a>,
<a href="https://pubchem.ncbi.nlm.nih.gov//edit3/index.html">PubChem Sketcher</a>, and
<a href="https://www.rcsb.org/chemical-sketch">Chemical Sketch</a>. We used ChemDraw for
its flexibility in the structure manipulation and because our University
has an institutional subscription (so its free for us), but overall,
for the option to export the chemical structure as .pdf or .svg
that we can use with more flexibility later in the process of image processing.</p>
<p>For this example, we are providing the .tiff chemical structure that we exported
using chemdraw:</p>
<ul>
<li><a href="https://gitlab.com/DanielQuiroz97/danielquiroz97.gitlab.io/-/blob/master/content/post/2023-09-29-making-ms-ms-mirror-plots-to-compare-experimetnal-vs-reference-spectra/img/Isoschaftoside_structure.tiff?ref_type=heads">isoschaftoside structure</a></li>
<li><a href="https://gitlab.com/DanielQuiroz97/danielquiroz97.gitlab.io/-/blob/master/content/post/2023-09-29-making-ms-ms-mirror-plots-to-compare-experimetnal-vs-reference-spectra/img/Nictoflorin_structure.tiff?ref_type=heads">nictoflorin structure</a></li>
</ul>
<p>First, we need to import image, and then we add the image on top of the mirror
plot.</p>
<pre class="r"><code>isos_structure &lt;- image_read(&quot;img/Isoschaftoside_structure.tiff&quot;) 

# Use cowplot funcition and use isos_mirror as base plot
isos_mirror_final &lt;- ggdraw(isos_mirror) + 
  draw_image(isos_structure, 
             x = -0.2, # Relative x position
             y = 0, # Relative y position
             scale = 0.4)  # Scaling to fit in the mirror plot
isos_mirror_final</code></pre>
<p><img src="unnamed-chunk-18-1.png" width="672" /></p>
<p>Finally, you can export your publication-ready mirror plot having control
over the image size, and resolution with <code>ggsave()</code>.
You can set parameters based on your needs, but here it is an example of
what we used here.</p>
<pre class="r"><code># svg format
ggsave(plot =  isos_mirror_final, filename = &quot;nictoflorin_mirror.svg&quot;,
       dpi = 300) # Resolution

# pdf format
ggsave(plot =  isos_mirror_final, filename = &quot;nictoflorin_mirror.pdf&quot;,
       dpi = 300, # resolution
       width = 4, height = 2, units = &quot;cm&quot;, # image size
       scale = 5) # image scale
</code></pre>
<p>If you reached this point of the document, you reached the end of this tutorial.
Please, let us know if this tutorial was useful and your feedback by clicking
the email icon at the button of this page.</p>
</div>
</div>
