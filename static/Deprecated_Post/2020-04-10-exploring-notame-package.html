---
title: Exploring the notame package
author: Daniel Quiroz
date: '2020-04-10'
slug: notame
categories:
  - LC-MS
tags:
  - Untarget metabolomics
  - Liquid chromatography
  - Mass spectrometry
subtitle: 'Part I: Preprocessing'
description: 'notame is an R package for untargeted metabolomic profiling. This package offers common statistical analysis for metabolomics such as preprocessing, feature-wise analysis, multivariate analysis and visualizations'
image: '/img/post/qc_white.png'
output:
  blogdown::html_page:
    toc: true
---

<script src="/rmarkdown-libs/header-attrs/header-attrs.js"></script>

<div id="TOC">
<ul>
<li><a href="#disclaimer">Disclaimer</a></li>
<li><a href="#notame-workflow-introduction">Notame workflow introduction</a></li>
<li><a href="#package-installation">Package installation</a></li>
<li><a href="#ms-dial-analysis">MS-DIAL analysis</a>
<ul>
<li><a href="#peak-table-modifications">Peak table modifications</a></li>
</ul></li>
<li><a href="#notame-workflow">Notame workflow</a>
<ul>
<li><a href="#log-file">Log file</a></li>
<li><a href="#importing-data">Importing data</a></li>
<li><a href="#preprocessing">Preprocessing</a></li>
<li><a href="#batch-correction">Batch correction</a></li>
</ul></li>
</ul>
</div>

<style>
body {text-align: justify}
</style>
<div id="disclaimer" class="section level2">
<h2>Disclaimer</h2>
<ul>
<li><p><font color="e96060"> This post does not intend to replace the official
package documentation. This post aims to examine the main functions that
it provides. For official documentation, please refer to the paper or the
corresponding author. </font></p></li>
<li><p><font color="00c018"> In order to avoid package misuse, this post
was revised for the package developer and his suggestions was already
implemented. </font></p></li>
</ul>
</div>
<div id="notame-workflow-introduction" class="section level2">
<h2>Notame workflow introduction</h2>
<p>The <em>notame</em> package is documented and explained in a detailed manner in the paper
<a href="https://doi:10.3390/metabo10040135">“notame”: Workflow for Non-Targeted LC–MS Metabolic Profiling</a>.
This package can be found, at the moment, only at
<a href="https://github.com/antonvsdata/notame">github</a> in the current 0.0.1 version.</p>
<p>Briefly, the notame package works with the output of the
<a href="http://prime.psc.riken.jp/compms/msdial/main.html">MS-DIAL</a> software at the
MS1 level. MS-DIAL is used to perform the peak picking and peak alignment
routines before to imports the peak list in notame package.</p>
<p>The notame package offers more than 100 functions to perform metabolomic
profiling analysis. This functions cover several approach such as:</p>
<ul>
<li>Import data</li>
<li>Preprocessing</li>
<li>Feature-wise analysis (Parametric and non-parametric analysis)</li>
<li>Multivariate analysis</li>
<li>Visualize statistical analysis and results</li>
</ul>
</div>
<div id="package-installation" class="section level2">
<h2>Package installation</h2>
<p>Since the package is only hosted in github, it must be installed through the
developer version with the <code>devtools</code> package.</p>
<p>If you do not have the <code>devtools</code> package, you can use the following commands:</p>
<pre class="r"><code>install.packages(&quot;devtools&quot;)
devtools::install_github(&quot;antonvsdata/notame&quot;,
                         build_manual = T, build_vignettes = T)</code></pre>
<p>The <code>install_github</code> function will download the package, and install it in your
computer. In this case, the <code>build_manual</code> and <code>build_vignettes</code> are set to
<code>TRUE</code> in order to install the package documentation and available tutorials.</p>
</div>
<div id="ms-dial-analysis" class="section level2">
<h2>MS-DIAL analysis</h2>
<p>Although several peak picking algorithms exist in vendor and non-vendor
software, the notame package is personalized to work primarly, but not
exclusively, with MS-DIAL.</p>
<p>In the notame <a href="https://doi:10.3390/metabo10040135">paper</a>, the parameters to be
used in MS-DIAL from the <span class="math inline">\(30^{th}\)</span> to the <span class="math inline">\(33^{rd}\)</span> step are described.
Briefly, we include the suggested parameters with some changes based on our own
data characteristics.</p>
<div class="figure">
<img src="/post/2020-04-10-exploring-notame-package_files/MSDIAL.png" style="width:75.0%" alt="" />
<p class="caption">Figure 1. Parameters used for peak picking and aligment.</p>
</div>
<div id="peak-table-modifications" class="section level3">
<h3>Peak table modifications</h3>
<p>Once MS-DIAL finishes the process, the alignment results has to be exported in
a .txt file. As explained in the <code>project_example</code> vignette, you have to
slightly modify this table and transform to an excel file. To have a big
picture about how this table should be like, notame authors provide some
<a href="https://github.com/antonvsdata/notame/tree/master/inst/extdata">example data</a>.</p>
<p>From the raw MS-DIAL output (.txt), you have to introduce some modifications,
which most of them are renaming column, For example,</p>
<table>
<thead>
<tr class="header">
<th align="left">Old name</th>
<th align="left">New name</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Alignment ID</td>
<td align="left">Compound</td>
</tr>
<tr class="even">
<td align="left">Average Rt(min)</td>
<td align="left">RT</td>
</tr>
<tr class="odd">
<td align="left">Average Mz</td>
<td align="left">Mass</td>
</tr>
</tbody>
</table>
<p>Additionally, two columns must be added before you import your data.
A column named as <em>“Column”</em> and <em>“Ion Mode”</em> has to be created. As suggestion,
you can replace one of the column in your raw data that does not contain
information. These two columns are in case if you analyzed your samples with
multiple column types (<span class="math inline">\(C_{18}\)</span>, <span class="math inline">\(C_{8}\)</span>, …) or the ionization was performed
in different modes. If you only acquired your data with one column and one ion
mode, create the column and fill all the cells with the same value. Then, export
the table in an excel file with only one sheet.</p>
<p>Your final table should look similar as the next table.</p>
<div class="figure">
<img src="/post/2020-04-10-exploring-notame-package_files/example_data.png" style="width:60.0%" alt="" />
<p class="caption">Figure 2. Representation of the example data provided by the notame package authors.</p>
</div>
<p>The table is composed with three embedded tables. The sample
metadata (top blue), the chromatographic information (left green) and the
feature ambundance (middle orange).</p>
</div>
</div>
<div id="notame-workflow" class="section level2">
<h2>Notame workflow</h2>
<p>Here we are going to load the required libraries to perform data analysis.</p>
<pre class="r"><code>library(notame) # Workflow for non-targeted LC-MS metabolic profiling 
library(doParallel) # Foreach Parallel Adaptor for the &#39;parallel&#39; Package
library(here) # A Simpler Way to Find Your Files 
library(magrittr) # A Forward-Pipe Operator for R 
library(tidyverse) # Easily Install and Load the &#39;Tidyverse&#39; 
library(patchwork) # The Composer of Plots 
library(BatchCorrMetabolomics) # &quot;Improved Batch Correction in Untargeted MS-Based Metabolomics&quot;</code></pre>
<div id="log-file" class="section level3">
<h3>Log file</h3>
<p>In order to trace every process, the notame package includes a log system to
trace every single command. Therefore, users can see the log file to check
the executed commands.</p>
<pre class="r"><code>ppath &lt;- here()
init_log(log_file = paste0(ppath, &#39;log.txt&#39;))</code></pre>
</div>
<div id="importing-data" class="section level3">
<h3>Importing data</h3>
<p>Data can be imported with the <code>read_from_excel</code> function. The default argument values
are intuitive to read the data only with one extra argument than the file name.</p>
<pre class="r"><code>data &lt;- read_from_excel(file = here(&quot;static&quot;, &quot;data&quot;, &quot;peak_noblanck.xlsx&quot;), 
                        split_by = c(&quot;Column&quot;, &quot;Ion mode&quot;))</code></pre>
<p>Once the data is read, a <em>MetaboSet</em> has to be created in order to create a
specific R object (S4). At the first glance, the MetaboSet is composed by three
tables:</p>
<ul>
<li><em>exprs</em>: The expression table which contains the feature abundance,</li>
<li><em>pheno_data</em>: the phenotype or a more coloquial, the metadata, and</li>
<li><em>feature_data</em>: The chromotographic information such as rt, m/z, etc.</li>
</ul>
<div class="figure">
<img src="/post/2020-04-10-exploring-notame-package_files/Metaboset.jpg" style="width:60.0%" alt="" />
<p class="caption">Figure 3. MetaboSet architecture.</p>
</div>
<p>The MetaboSet object is created as follow:</p>
<pre class="r"><code>modes &lt;- construct_metabosets(exprs = data$exprs, pheno_data = data$pheno_data,
                              feature_data = data$feature_data, group_col = &quot;Class&quot;)</code></pre>
<p><strong>Note</strong>: the <code>group_col</code> argument is a string with the column name of the
sample class, ensure to provide the correct column name based on the Fig. 2.</p>
<p>FInally, each mode (set of columns and ion mode) can be extracted in a single
object.</p>
<pre class="r"><code>mode &lt;- modes$Octodecyl_Pos</code></pre>
<p>As you can probably notice, this samples was acquiered with a <span class="math inline">\(C_{18}\)</span> column
in a positive ionization mode.</p>
<p>In the notame package, the function <code>visualizations</code> create all available figures
for the MetaboSet object. Although, in this document this function is not use,
I totally encourage to use it in order to inspect the processing routines.</p>
<pre class="r"><code>raw_sambx &lt;- plot_sample_boxplots(mode, order_by = &quot;Injection_order&quot;)
raw_pca &lt;- plot_pca(mode, center = T, color = &quot;Batch_ID&quot;) 
raw_pca + raw_sambx + plot_layout(guides = &quot;collect&quot;)</code></pre>
<p><img src="/post/2020-04-10-exploring-notame-package_files/figure-html/unnamed-chunk-8-1.png" width="672" /></p>
<p>The Principal Component Analysis (PCA) in the left shows a clear separation
between the samples classes. I would like to make emphasis in the QC injections
which are not properly cluster in a common principal component space. They
were projected with a descended slope trend. This is a clear example of the lack
of drift correction and common issues as batch correction. Additionally,
as example of the influence of the injection order in the acquired mass
spectra, the boxplot has a descendant trend in the abundance of the features.</p>
</div>
<div id="preprocessing" class="section level3">
<h3>Preprocessing</h3>
<p>The first step of the preprocessing is to change the features with value equal
to 0 to <code>NA</code>, since the default behavior of the MS-DIAL is to replace <code>NA</code>
with 0.</p>
<pre class="r"><code>mode &lt;- mark_nas(mode, value = 0)</code></pre>
<p>Then, features with low detection rate are first detected and then removed.
The notame package employs two criteria to select this features. First, is the
feature presence in a percentage of QC injections, and then the feature presence
in a percentage within a sample group or class. For example, in the next
command, features which that were not detected in the 75% of the QC injections
and 80% of sample groups will be discarded.</p>
<pre class="r"><code>mode &lt;- flag_detection(mode, qc_limit = 0.75, group_limit = 0.8)</code></pre>
<pre><code>## 
## 4% of features flagged for low detection rate</code></pre>
<p>Once the features with low detection rate are detected, the correction drift can
be applied by smoothed cubic spline regression. It is done for each
feature.</p>
<pre class="r"><code>dc &lt;- dc_cubic_spline(mode)</code></pre>
<pre><code>## 
## Starting drift correction at 2020-05-17 16:50:08</code></pre>
<pre><code>## Warning: executing %dopar% sequentially: no parallel backend registered</code></pre>
<pre><code>## Drift correction performed at 2020-05-17 16:50:09</code></pre>
<pre class="r"><code>corrected &lt;- dc$object
corrected &lt;- flag_quality(corrected)</code></pre>
<pre><code>## 
## 89% of features flagged for low quality</code></pre>
<pre class="r"><code>corr_sambx &lt;- plot_sample_boxplots(corrected, order_by = &quot;Injection_order&quot;)
corr_pca &lt;- plot_pca(corrected, center = T) 
corr_pca + corr_sambx + plot_layout(guides = &quot;collect&quot;)</code></pre>
<p><img src="/post/2020-04-10-exploring-notame-package_files/figure-html/unnamed-chunk-12-1.png" width="672" /></p>
</div>
<div id="batch-correction" class="section level3">
<h3>Batch correction</h3>
<p>The batch correction implemented in the notame package is supported by the
<a href="https://gitlab.com/CarlBrunius/batchCorr/">batchCorr</a>,
<a href="https://bioconductor.org/packages/release/bioc/html/RUVSeq.html">RUVseq</a>,
and <a href="https://github.com/rwehrens/BatchCorrMetabolomics">BatchCorrMetabolomics</a> packages.
The later package corrects the batch effect by fitting ancova models, with batches
as factorial information. Additionally, different regression models are
available, such as robust linear regression <code>method = "rlm"</code> and
censored regression <code>method = "tobit"</code>. The
<code>BatchCorrMetabolomics</code> package is detailed described in the
<a href="https://www.ncbi.nlm.nih.gom/pmc/articles/PMC4796354/">Roben Wehrens and collegues paper</a>.</p>
<p>In this case, the robust linear regression will be used in order to avoid the
possible outlaiers that may have considerable effect in the linear model.</p>
<pre class="r"><code>corrected_batch &lt;- dobc(object = corrected, batch = &quot;Batch_ID&quot;,
                        ref = &quot;QC&quot;, ref_label = &quot;QC&quot;, method = &quot;rlm&quot;)</code></pre>
<pre class="r"><code>batch_sambx &lt;- plot_sample_boxplots(corrected_batch, order_by = &quot;Injection_order&quot;)
batch_pca &lt;- plot_pca(corrected_batch, center = T, color = &quot;Batch_ID&quot;, shape = &quot;Class&quot;) 
batch_pca + batch_sambx + plot_layout(guides = &quot;collect&quot;)</code></pre>
<p><img src="/post/2020-04-10-exploring-notame-package_files/figure-html/unnamed-chunk-14-1.png" width="672" /></p>
<p>The PCA may not be the best approach to evaluate the impact of the batch
correction. In this case, hierarchical clustering will be used to assess the
batch correction.</p>
<pre class="r"><code>plot_dendrogram(corrected)</code></pre>
<p><img src="/post/2020-04-10-exploring-notame-package_files/figure-html/unnamed-chunk-15-1.png" width="672" /></p>
<p>This dendogram is performed with a not batch corrected data. The batch
injections are clustered in the right in an upper level.</p>
<pre class="r"><code>plot_dendrogram(corrected_batch)</code></pre>
<p><img src="/post/2020-04-10-exploring-notame-package_files/figure-html/unnamed-chunk-16-1.png" width="672" /></p>
<p>The main difference with the batch corrected data is the batch injections are
clustered in a lower dendogram level. This is a clear example of the batch
correction affects the similarity of batch injections, which lead to a deeper
dendogram level.</p>
<p>I would like to finis this post at this point since the QC injections helped to
evaluate the impact of each preprocessing routine. In the next post, the QC
will be dropped to continue with the analysis.</p>
</div>
</div>
